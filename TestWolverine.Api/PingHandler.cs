using Marten;
using Wolverine.Attributes;
using Wolverine;
using Newtonsoft.Json;

namespace Ponger;
public class PingCommand
{
    public int Number1 { get; set; }
    public int Number2 { get; set; }
}
public class PingCommandResult
{
    public int NumberResult { get; set; }
}

public class PingHandler
{
    [Transactional]
    public static PingCommandResult Handle(
        // This would be the message
        PingCommand command,

        // Any other arguments are assumed
        // to be service dependencies
        IDocumentSession session
       ,IMessageBus mesgbus
       ,IMessageContext context
    )
    {
        Console.WriteLine("Sending pingMessage");

        /*var sendTask =  mesgbus.InvokeAsync<Ponger.PongMessage>(new Ponger.PingMessage(){
            Number1 = command.Number1,
            Number2 = command.Number2
        });
        sendTask.Wait();
        Console.WriteLine(JsonConvert.SerializeObject(sendTask.Result));*/

        

        var sendTask =  mesgbus.SendAsync(new Ponger.PingMessage(){
            Number1 = command.Number1,
            Number2 = command.Number2
        });
        
        Console.WriteLine("done send pingmessage");

        return new PingCommandResult();
    }
}