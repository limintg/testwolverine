using Oakton;
using Marten;
using Marten.AspNetCore;
using Marten.Events.Projections;
using System;
using Wolverine;
using Wolverine.Marten;
using Wolverine.RabbitMQ;

var builder = WebApplication.CreateBuilder(args);
builder.Host.ApplyOaktonExtensions();
builder.Services.AddMarten(opts =>
    {
        var connectionString = builder
            .Configuration
            .GetConnectionString("postgres");

        opts.Connection(connectionString);
        opts.DatabaseSchemaName = "public";
        opts.Projections.Add<TestEventSourcingProjector>(ProjectionLifecycle.Async);
        
        //indexing
        opts.Schema.For<TestRecord>().Index(x => x.Code);

        opts.Schema.For<TestEventSourcingPj>().Index(x => x.EvName);
    })
    // Optionally add Marten/Postgresql integration
    // with Wolverine's outbox
    .IntegrateWithWolverine()
    .ApplyAllDatabaseChangesOnStartup()
    .UseLightweightSessions();

builder.Services.AddMartenStore<ITestStore>(opts => {
    opts.Connection("Host=localhost;Port=5432;Database=test_db;Username=postgres;password=Ubvu5wm4sf91Hhf");
    opts.DatabaseSchemaName = "public";
})
.ApplyAllDatabaseChangesOnStartup();

builder.Host.UseWolverine(opts =>
{
    // I've added persistent inbox
    // behavior to the "important"
    // local queue
    //opts.LocalQueue("important")
    //    .UseDurableInbox();
    

    //opts.PublishMessage<Ponger.PingMessage>().ToRabbitExchange("test_ping_pong");
    
    
    /*
    opts.UseRabbitMq(rabbit =>
    {
        rabbit.HostName = "localhost";
        rabbit.Port = 5672;
        rabbit.UserName = "user";
        rabbit.Password = "password";
    })
    .DeclareExchange("test_ping_pong", exchange =>
        {
            // Also declares the queue too
            exchange.BindQueue("test_pongs");
        })
    .AutoProvision();*/

    opts.UseRabbitMq(rabbit =>
    {
        rabbit.HostName = "localhost";
        rabbit.Port = 5672;
        rabbit.UserName = "user";
        rabbit.Password = "password";
    })
    .UseConventionalRouting()
    .AutoProvision() 
    .AutoPurgeOnStartup()
    .ConfigureSenders(x => x.UseDurableOutbox());
});

//builder.Services.AddMvc();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

//app.MapPost("/test/create", (TestRecord body, IMessageBus bus) => bus.InvokeAsync(body));

app.MapPost("/test/create", async (TestRecord body, IDocumentSession session) => {
    session.Store(body);

    await session.SaveChangesAsync();
    return "created";
});

app.MapPost("/test_store/create", async (TestRecordType2 body, ITestStore astore) => {
    using (var aSessesion = astore.LightweightSession())
    {
        aSessesion.Store(body);
        aSessesion.SaveChanges();
    }
    return "created";
});

app.MapGet("/", () => Results.Redirect("/swagger"));




app.MapPost("/memo/add" , async (EventAddMemo ev , IDocumentSession session , IMessageBus mesgbus ) => {
    var aresult = session.Events.StartStream(
        ev
    );
    session.SaveChanges();
    //var todo = await mesgbus.InvokeAsync<Todo>(ev);
    //return "stream id ["+aresult.Id+"] Todo ["+todo.Id+"]";
    return "stream id ["+aresult.Id+"]";
});

app.MapPost("/memo/append" , async (Guid Id,EventAddMemo ev , IDocumentSession session) => {
    var aresult = session.Events.Append(
        Id,
        ev
    );
    session.SaveChanges();
    return "stream id : "+aresult.Id;
});

app.MapGet("/memo/list" , async (IDocumentSession session) => {
    var aggRsl = session.Events.AggregateStream<TestEventSourcing>(new Guid());
    return aggRsl;
});


app.MapGet("/memo/list_projections" , async (IDocumentSession session) => {
    var aggRsl = session.LoadAsync<TestEventSourcingPj>(new Guid("018de9ac-46cb-4277-a6ef-ef7bcefcefe0"));
    aggRsl.Wait();
    return aggRsl.Result;
});


app.MapGet("/memo/list_by_id" , async (Guid Id,IDocumentSession session) => {
    var aggRsl = session.Events.AggregateStream<TestEventSourcing>(Id);
    return aggRsl;
});

/*
app.MapPost("/testitem/add" , async (TestItemAdd ev , IMessageBus mesgbus ) => {
    var rsl = mesgbus.InvokeAsync<TestItemAdded>(ev) ;
    rsl.Wait();
    return rsl.Result;
});
*/

app.MapGet("/testitem/list" , async (IMessageBus mesgbus) => {
    var rsl = mesgbus.InvokeAsync<List<TestItem>>(new TestItemGetList());
    rsl.Wait();
    return rsl.Result;
});

app.MapPost("/ping" , async (Ponger.PingCommand command,IMessageBus mesgbus) => {
    var rsl = mesgbus.InvokeAsync<Ponger.PingCommandResult>(command);
    rsl.Wait();
    return rsl.Result;
});

app.MapPost("/ping2" , async (Ponger.PingCommand command,IMessageBus mesgbus , IMessageContext mesgCont) => {
    //var rsl = await mesgbus.InvokeAsync<Ponger.PingCommandResult>(command);
    /*var rsl = await mesgbus.InvokeAsync<Ponger.PongMessage>(new Ponger.PingMessage(){
            Number1 = command.Number1,
            Number2 = command.Number2
    });*/
    
    /*await mesgbus.InvokeAsync(new Ponger.PingMessage(){
            Number1 = command.Number1,
            Number2 = command.Number2
    });
    
    return "";*/

    /*var rsl = await mesgbus.InvokeAsync<Ponger.PongMessage>(new Ponger.PingMessage(){
            Number1 = command.Number1,
            Number2 = command.Number2
    });
    return rsl;*/

    var rsl = await mesgCont.InvokeAsync<Ponger.PongMessage>(new Ponger.PingMessage(){
            Number1 = command.Number1,
            Number2 = command.Number2
    });
    return rsl;
});

Console.WriteLine(new Guid());
Console.WriteLine(Guid.NewGuid());

app.UseSwagger();
app.UseSwaggerUI();

return await app.RunOaktonCommands(args);