using Marten;
using Marten.Events.Aggregation;
using System.Collections.Generic;

public record EventAddMemo(string Value);

public class TestEventSourcing
{
    public Guid Id {get;set;}
    public List<string> Memos {get;set;} = new List<string>();
    public void Apply(EventAddMemo ev)
    {
        Memos.Add(ev.Value);
    }
}

public class TestEventSourcingPj
{
    public Guid Id {get;set;}
    public string EvName {get;set;}
    public int MemoCount {get;set;} = 0;
    
}

public class TestEventSourcingProjector : SingleStreamProjection<TestEventSourcingPj>
{
    public void Apply(TestEventSourcingPj snapshot, EventAddMemo ev)
    {
        snapshot.MemoCount += 1;
    }
}