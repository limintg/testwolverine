using Microsoft.AspNetCore.Mvc;
using Wolverine.EntityFrameworkCore;
using Wolverine;

public class TestItemController : ControllerBase
{
    [HttpPost("/testitem/add")]
    public async Task<TestItemAdded> testitem_add(
        [FromBody] TestItemAdd command,
        [FromServices] IMessageBus mesgbus
    )
    {
        var rsl = mesgbus.InvokeAsync<TestItemAdded>(command) ;
        rsl.Wait();
        return rsl.Result;
    }
}