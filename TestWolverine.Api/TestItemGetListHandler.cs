using Marten;
using Wolverine.Attributes;
using System.Collections.Generic;

public class TestItemGetListHandler
{
    [Transactional]
    public static List<TestItem> Handle(
        TestItemGetList command,
        IDocumentSession session
    )
    {
        List<TestItem> rsl = new List<TestItem>();
        rsl.Add(new TestItem(new Guid(),"aaa"));
        rsl.Add(new TestItem(new Guid(),"bbb"));
        rsl.Add(new TestItem(new Guid(),"ccc"));
        return rsl;
    }
}