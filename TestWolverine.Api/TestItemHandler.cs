using Marten;
using Wolverine.Attributes;
using System.Collections.Generic;
using Wolverine;
using System.Net.Http.Headers;

public class TestItemHandler
{
    [Transactional]
    public static TestItemAdded Handle(
        // This would be the message
        TestItemAdd command,

        // Any other arguments are assumed
        // to be service dependencies
        IDocumentSession session
        //,IMessageBus mesgbus
    )
    {
        //var catalogTask = mesgbus.InvokeAsync<CommandGetCatalogResult>(new CommandGetCatalog() {Product = "abc"});
        //catalogTask.Wait();

        var newItem = new TestItem( Guid.NewGuid() , command.Name);
        var aresult = session.Events.StartStream(
            newItem
        );
        session.SaveChanges();

        return new TestItemAdded(){
            Name = newItem.Name,
            Id = newItem.Id,
            StreamID = aresult.Id.ToString()
        };
    }
}