
using Wolverine.Attributes;

namespace Ponger;

[MessageIdentity("test_ping")]
public class PingMessage
{
    public int Number1 { get; set; }
    public int Number2 { get; set; }
}

[MessageIdentity("test_pong")]
public class PongMessage
{
    public int NumberResult { get; set; }
}