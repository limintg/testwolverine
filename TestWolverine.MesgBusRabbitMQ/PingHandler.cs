using Newtonsoft.Json;
using Spectre.Console;
using Wolverine;

namespace Ponger;

#region sample_PingHandler

public static class PingHandler
{
    // Simple message handler for the PingMessage message type
    /*public static ValueTask Handle(
        // The first argument is assumed to be the message type
        PingMessage message,

        // Wolverine supports method injection similar to ASP.Net Core MVC
        // In this case though, IMessageContext is scoped to the message
        // being handled
        IMessageContext context)
    {
        AnsiConsole.Write($"[blue]Got ping #{message.Number1} #{message.Number2} [/]");
        Console.WriteLine($"[blue]Got ping #{message.Number1} #{message.Number2} [/]");
        var response = new PongMessage
        {
            NumberResult = message.Number1 + message.Number2
        };

        // This usage will send the response message
        // back to the original sender. Wolverine uses message
        // headers to embed the reply address for exactly
        // this use case
        return context.RespondToSenderAsync(response);
    }*/
    public static void Handle(
        PingMessage message,
        IMessageContext context
    )
    {
        Console.WriteLine("PingMessage toggle "+JsonConvert.SerializeObject(message));
        var rsl = new PongMessage(){
            NumberResult = message.Number1 + message.Number2
        };
        Console.WriteLine("Reply -> "+JsonConvert.SerializeObject(rsl));
        context.RespondToSenderAsync(rsl);
    }
}

#endregion