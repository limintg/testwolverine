
/*using TestWolverine.MesgBusRabbitMQ;

var builder = Host.CreateApplicationBuilder(args);
builder.Services.AddHostedService<Worker>();

var host = builder.Build();
host.Run();*/
using Oakton;
using Wolverine;
using Wolverine.RabbitMQ;
using Wolverine.SqlServer;

var builder = Host.CreateDefaultBuilder(args);
    builder.UseWolverine(opts =>
    {
        
        //opts.PublishMessage<Ponger.PongMessage>().ToRabbitExchange("test_ping_pong");
        opts.ListenToRabbitQueue("test_pings").UseForReplies();

        // Configure Rabbit MQ connections and optionally declare Rabbit MQ
        // objects through an extension method on WolverineOptions.Endpoints
        
        /*opts.UseRabbitMq(rabbit => {
            rabbit.HostName = "localhost";
            rabbit.Port = 5672;
            rabbit.UserName = "user";
            rabbit.Password = "password";
        }) 
        // This is short hand to connect locally
        .DeclareExchange("test_ping_pong", exchange =>
        {
            // Also declares the queue too
            exchange.BindQueue("test_pings");
        })
        .AutoProvision()


        // Option to blow away existing messages in
        // all queues on application startup
        .AutoPurgeOnStartup();*/

        opts.UseRabbitMq(rabbit =>
        {
            rabbit.HostName = "localhost";
            rabbit.Port = 5672;
            rabbit.UserName = "user";
            rabbit.Password = "password";
        })
        .UseConventionalRouting()
        .AutoProvision() 
        .AutoPurgeOnStartup()
        .ConfigureSenders(x => x.UseDurableOutbox());
    });

return await builder.RunOaktonCommands(args);