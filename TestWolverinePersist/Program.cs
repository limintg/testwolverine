﻿using Oakton;
using Marten;
using Oakton.Resources;
using Wolverine;
using Wolverine.Http;
using Microsoft.OpenApi.Models;
using Wolverine.Marten;
using Wolverine.Postgresql;
using Wolverine.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);
builder.Host.ApplyOaktonExtensions();

builder.Services.AddMarten(opts => 
    {

    }
)
.UseLightweightSessions();

builder.Host.UseWolverine(opts =>
{
    //Setting up Server-backed message storage
    opts.PersistMessagesWithPostgresql(
        "Host=vps-843fba86.vps.ovh.ca;Port=5432;Database=test_persist;Username=postgres;password=lEgSk618C3IRcHt7y7oF"
    );

    // Set up Entity Framework Core as the support
    // for Wolverine's transactional middleware
    opts.UseEntityFrameworkCoreTransactions();
    
    // Enrolling all local queues into the
    // durable inbox/outbox processing
    opts.Policies.UseDurableLocalQueues();

    opts.PublishMessage<TestCommand>().ToLocalQueue("testcommand_que");
});

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

app.MapGet("/", () => Results.Redirect("/swagger"));

app.MapPost("/test" , async (TestCommand cmd , IMessageBus mesgbus) => {
    var rsl = mesgbus.InvokeAsync<TestCommandResult>(cmd);
    rsl.Wait();
    return rsl.Result;
});

app.UseSwagger();
app.UseSwaggerUI();


return await app.RunOaktonCommands(args);


