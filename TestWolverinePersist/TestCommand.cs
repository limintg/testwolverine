using Wolverine;
using Wolverine.Attributes;

public record TestCommand (string Command);

public record TestCommandResult (string Result);


public class TestCommandHandler
{
    [Transactional]
    public static TestCommandResult Handle(
        // This would be the message
        TestCommand command,
        IConfiguration configuration,
        IMessageBus _mesgBus
    )
    {
        //throw new Exception("try error");
        Console.WriteLine("TestCommandHandler.Handle");
        return new TestCommandResult("");
    }
}